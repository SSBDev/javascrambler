import java.util.Scanner;

class Main {
  public static void main(String[] args)throws InterruptedException{
    Scanner in = new Scanner(System.in);
    System.out.println("What is the message?");
       String message = in.nextLine();
    System.out.println("Would you like to scramble a message or descramble the message?");
    System.out.println("Enter 'd' if you would like to descramble and 's' if you would like to scramble, using your keyboard.");
      char scr = in.nextLine().charAt(0);
    String finalMessage = "";
    if(scr == 'd'){
      finalMessage = descramble(message);
    }
    else if(scr == 's'){
      finalMessage = scramble(message);
    }
    
    System.out.println("The message is: ");
    System.out.println(finalMessage);
  }
  public static String descramble(String message){
    String str = new String(); 
    String retMess = "";
      String deScrambledMessage;
      char[] deScrambledMessageArray = new char[message.length()];
      //Split up the message
      for(int i = 0; i < message.length(); i++){
        deScrambledMessageArray[i] = message.charAt(i);
      }
      char temp;
      //Mix up (+2)
      for(int k = 0; k < message.length()-1; k++){
        k++;
          temp = deScrambledMessageArray[k-1];
          deScrambledMessageArray[k-1] = deScrambledMessageArray[k];
          deScrambledMessageArray[k] = temp;
      }
      //Create a Copy
      char [] deCop = new char [deScrambledMessageArray.length];
      System.arraycopy(deScrambledMessageArray, 0, deCop, 0, message.length());
      //Reverse the Message
      for(int j = 0; j < message.length(); j++){
        deScrambledMessageArray[j] = deCop[message.length()-j-1];
      }
      deScrambledMessage = str.valueOf(deScrambledMessageArray);
      //System.out.println(deScrambledMessage);
      retMess = deScrambledMessage;
    return retMess;
  }
  public static String scramble(String message){
    String retMess = "";
    String str = new String(); 
    String scrambledMessage;
    char[] scrambledMessageArray = new char[message.length()];
    //Split up the meesage
    char[] messageSplit = new char[message.length()];
    for(int i = 0; i < message.length(); i++){
      messageSplit[i] = message.charAt(i);
    }
    //Reverse the Message
    for(int j = 0; j < message.length(); j++){
      scrambledMessageArray[j] = message.charAt(message.length()-j-1);
    }
    //Mix up (+2)
    char temp;
    for(int k = 0; k < message.length()-1; k++){
      k++;
      temp = scrambledMessageArray[k];
      scrambledMessageArray[k] = scrambledMessageArray[k-1];
      scrambledMessageArray[k-1] = temp;
    }
    scrambledMessage = str.valueOf(scrambledMessageArray);
    retMess = scrambledMessage;
    return retMess;
  }
  public static void clear_wait(int wait)throws InterruptedException {
    Thread.sleep(wait);
    System.out.print("\033[H\033[2J");
    System.out.flush();
  }
}